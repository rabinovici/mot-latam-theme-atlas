/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const colors = require('colors');

console.log(colors.blue(`
   ------- Welcome -------
`));
console.log(colors.blue('   Please now run: ') + colors.yellow('gulp dev'));

console.log(colors.blue(`   and after you finish coding
   remember to run ` +  colors.yellow('gulp dist') + `
`));
