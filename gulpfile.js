/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const colors = require('colors');
const fs = require('fs-extra');
const gulpgit = require('gulp-git');
const prompt = require('gulp-prompt');
/* */

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Include files from gulp/ folder
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 **/
const _sass = require('./.gulp-tasks/sass');
const _js = require('./.gulp-tasks/js');
const _html = require('./.gulp-tasks/html');
const _sync = require('./.gulp-tasks/sync');
const _del = require('./.gulp-tasks/del');
const _server = require('./.gulp-tasks/server');
const _notify = require('./.gulp-tasks/notifications');
const _compress = require('./.gulp-tasks/compress');
const _extras = require('./.gulp-tasks/extras');
const _themes = require('./.gulp-tasks/themes');
const _logSymbols = require('log-symbols');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Watch
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 **/

function watchFiles () {
  watch('./src/**/*.html', series(_preview, _server.reload, _notify.notifyDoneHtml));
  watch('./src/**/*.php', series(_del.delPhp, _preview, _server.reload, _notify.notifyDonePhp));
  watch('./src/**/*.json', series(_preview, _server.reload, _notify.notifyDoneJson));
  watch(['./src/*.scss', './src/sass/*.scss'], series(_del.delCss, _preview, _server.reload, _notify.notifyDoneCss));
  watch(['./src/*.js', './src/js/*.js'], series(_del.delJs, _preview, _server.reload, _notify.notifyDoneJs));
  watch('./src/images/**/*', series(_del.delImg, _sync.syncImg, _server.reload, _notify.notifyDoneImg));
}

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Gulp Tasks List
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

function listTasks () {
  const $tasks = [
    '- Exit -',
    'dev',
    'dist',
    'theme',
    'serve',
    'distMin',
    'zip',
    'backup'
  ];

  return src('./')
    .pipe(prompt.prompt({
      type: 'list',
      name: 'task',
      message: colors.yellow('\n Please select ' + colors.red('task') + ' to run:'),
      choices: $tasks
    }, ($res) => {
      if ($res.task === '- Exit -' || $res.task === '- more -') {
        log(colors.red('\n   ⊗ No task selected\n'));
        return;
      }
      var selectedTask = $res.task;
      log('Running task: ' + selectedTask);
      series(selectedTask)($res);
    }));
}

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Gulp Tasks
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

const _x = series(
  _del.delSrc,
  _notify.notifyDone
);
exports.x = _x;

const _preview = series(
  _sync.syncTmp,
  _sass.compileSass,
  _js.compileJs,
  _html.htmlReplaceVars,
  _html.htmlInclude,
  _html.htmlInline,
  _html.htmlSearch,
  _extras.hiddenDist
);

exports.preview = _preview;

const _buildDist = series(
  _del.delTemp,
  _del.delDist,
  _sync.syncTmp,
  _preview,
  _html.htmlBeautify,
  _sync.syncDist,
  _notify.notifyDoneDist
);

const local = series(
  _del.delTemp,
  _sync.syncTmp,
  _del.delLocal,
  _preview,
  _html.htmlBeautify,
  _sync.syncLocal,
  _notify.notifyDoneLocal
);

const files = series(
  _del.delTemp,
  _sync.syncTmp,
  _del.delFiles,
  _sass.compileSass,
  _js.compileJs,
  _html.htmlReplaceVars,
  _html.htmlInclude,
  _html.htmlSearch,
  _html.htmlBeautify,
  _sync.syncFiles,
  _notify.notifyDoneFiles
);

const themes = series(
  _themes.backup,
  _themes.theme
);

exports.files = files;
exports.distForce = _buildDist;
exports.local = local;
exports.theme = themes;
exports.backup = _themes.backup;
exports.distMin = _compress.distMin;
exports.distMinInline = _compress.distMinInline;
exports.cc = _del.delAll;
exports.serve = _server.serverHtml;
exports.zip = _extras.zip;
exports.default = listTasks;

/**
 * ::::::: Dev
 */

function dev (done) {
  if (fs.existsSync('./src/.hidden')) {
    _themes.theme().then(function () {
      run();
    });
  } else {
    run();
  }
  return done();
}

const run = series(_preview, parallel(_server.serverHtml, watchFiles));

exports.dev = dev;
exports.saveTheme = _themes.saveTheme;
exports.devphp = series(_preview, parallel(_server.serverPhp, watchFiles));

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Build
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

/**
 * ::::::: Dist
 */
function build (done) {
  var $srcStatus = false;
  var $distStatus = false;
  var $status = '';

  if (fs.existsSync('./src/.hidden')) {
    log('');
    log(' ' + _logSymbols.info + ' ' + colors.green(' No files on your src/ folder  ') + '         ' + _logSymbols.info);
    log(' ' + _logSymbols.info + ' ' + colors.green(' Run ') + colors.red.bold('gulp themes ') + colors.green('and select your theme  ') + ' ' + _logSymbols.info);
    log('');
  } else {
    gulpgit.status({ args: '--porcelain', quiet: true }, (err, stdout) => {
      if (err) throw err;
      $srcStatus = stdout.includes('src/');
      $distStatus = stdout.includes('dist/');

      if ($srcStatus && !$distStatus) {
        $status = 'src-durty';
        log('');
        log(' ' + _logSymbols.warning + ' ' + colors.green('  Please commit your changes first on your ') + colors.red('src/ ') + colors.green('folder ') + ' ' + _logSymbols.warning);
        log('');
      }

      if (!$srcStatus && $distStatus) {
        $status = 'dist-durty';
        src('./dist/*')
          .pipe(gulpgit.add())
          .pipe(gulpgit.commit('Auto Commit - Changes only on DIST folder'));
      }

      if (!$srcStatus && !$distStatus) {
        $status = 'src-dist-clean';
        _buildDist();
      }

      if ($srcStatus && $distStatus) {
        $status = 'src-dist-durty';
        _buildDist();

        setTimeout(function () {
          log('');
          log(' ' + _logSymbols.warning + ' ' + colors.red('  Warning: you have changes on your ') + colors.yellow('src/') + colors.red(' and ') + colors.yellow('dist/ ') + colors.red('forlder ') + ' ' + _logSymbols.warning);
          log('');
        }, 2000);
      }
      // log($status);
    });
  }

  // _buildDist();
  return done();
}

exports.dist = build;
