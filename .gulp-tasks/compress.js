/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const htmlmin = require('gulp-htmlmin');
const minifyInline = require('gulp-minify-inline');
const plumber = require('gulp-plumber');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Compress
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

function distMin () {
  return src([
    './dist/*.html'
  ])
    .pipe(plumber())
    .pipe(htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      removeComments: true
    }))
    .pipe(dest('./dist-min'));
}

function distMinInline () {
  return src([
    './dist/*.html'
  ])
    .pipe(plumber())
    .pipe(minifyInline())
    .pipe(dest('./dist-min-inline'));
}

module.exports = {
  distMin,
  distMinInline,
}
