/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Sync
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

function syncTmp () {
  return src([
    './src/*/**/*',
    '!./src/sass/*',
    '!./src/js/*'
  ])
    .pipe(dest('./.tmp/'));
}

function syncDist () {
  return src([
    './.tmp/*/**/*',
    './.tmp/*.html',
    './.tmp/*.php',
    '!./.tmp/include/**/*',
    '!./.tmp/js/**/*',
    '!./.tmp/sass/**/*',
    '!./.tmp/maps/**/*',
    '!./.tmp/vendors/**/*',
    '!./.tmp/fonts/**/*',
    '!./.tmp/form/**/*',
    '!./.tmp/favicon/**/*'
  ])
    .pipe(dest('./dist/'));
}

function syncLocal () {
  return src([
    './.tmp/*/**/*',
    './.tmp/*.html',
    '!./.tmp/maps/**/*'
  ])
    .pipe(dest('./build-local/'));
}

function syncFiles () {
  return src([
    './.tmp/*/**/*',
    './.tmp/*.html',
    './.tmp/*.css',
    './.tmp/*.js',
    '!./.tmp/maps/**/*'
  ])
    .pipe(dest('./build-files/'));
}

function syncImg () {
  return src([
    './src/images/**/*'
  ])
    .pipe(dest('./.tmp/images/'));
}

module.exports =  {
  syncTmp,
  syncDist,
  syncLocal,
  syncFiles,
  syncImg,
}
