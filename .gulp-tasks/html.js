/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const beautifyCode = require('gulp-beautify-code');
const colors = require('colors');
const del = require('del');
const fileinclude = require('gulp-file-include');
const flatmap = require('gulp-flatmap');
const fs = require('fs-extra');
const inlinesource = require('gulp-inline-source');
const path = require('path');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const strip = require('gulp-strip-comments');
const token = require('gulp-token-replace');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Html
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

/** ::::::: HTML - Replace Vars */

function htmlReplaceVars () {
  return src([
    './src/*.html',
    './src/*.php'
  ])
    .pipe(plumber())
    .pipe(flatmap(function (stream, file) {
      var $list = file.path;
      var $filename = path.basename($list);
      var $name = path.parse($filename).name;
      var $filePath = '';
      if (fs.existsSync('./src/' + $name + '.json')) {
        $filePath = './src/' + $name + '.json';
      } else {
        $filePath = '../src/config.json';
      }
      delete require.cache[require.resolve($filePath)];
      var $config = require($filePath);
      $config.push = ({
        fileName: $name
      });
      return src([
        $list
      ])
        .pipe(plumber())
        .pipe(token({
          global: $config,
          prefix: '{% ',
          suffix: ' %}'
        }))
        .pipe(rename({
          prefix: '_'
        }))
        .pipe(dest('./.tmp/'));
    }));
}

/** ::::::: HTML - Include */

function htmlInclude () {
  return src([
    './.tmp/_*.html',
    './.tmp/_*.php'
  ])
    .pipe(plumber())
    .pipe(fileinclude())
    .pipe(rename({
      prefix: '-'
    }))
    .pipe(dest('./.tmp/'))
    .on('end', function () {
      del([
        './.tmp/_*.html',
        './.tmp/_*.php'
      ]);
    });
}

/** ::::::: HTML - Inline code */

function htmlInline () {
  return src([
    './.tmp/-_*.php',
    './.tmp/-_*.html'
  ])
    .pipe(plumber())
    .pipe(inlinesource({
      compress: false,
      swallowErrors: true
    }))
    .pipe(rename(function (opt) {
      opt.basename = opt.basename.replace(/-_/, '');
      return opt;
    }))
    .pipe(dest('./.tmp/'))
    .on('end', function () {
      del([
        './.tmp/-_*.html',
        './.tmp/-_*.php'
      ]);
    });
}

/** ::::::: HTML - Serch Urls */

function htmlSearch () {
  const fullURL = /((\w+:\/\/)[-a-zA-Z0-9:@;?&=/%+.*!'(),$_{}^~[\]`#|]+)/g;
  const $config = require('../src/config.json');
  return src([
    './src/*.html',
    './src/*.scss'
  ])
    .pipe(replace(fullURL, function (match) {
      const findUrl = $config.url_search;
      const rexUrl = /(http[s]?:\/\/?[^/\s]+)(.*)/g;
      const pattern = rexUrl.exec(match);

      if (match.indexOf('http://') !== -1 && match.indexOf(findUrl) === -1) {
        log(colors.yellow('░░░ ' + colors.white('USE HTTPS ') + 'This URL is not https:'));
        log(colors.white.bold('\n ➥  file: ' + this.file.relative));
        log(colors.blue(' x        ' + match + '\n'));
      }

      if (match.indexOf(findUrl) !== -1) {
        log(colors.yellow('░░░ ' + colors.white('USE HTTPS ') + '(Manually replace these URL in your src files for https)'));
        log(colors.white.bold('\n ➥  file: ' + this.file.relative));
        log(colors.red(' x        ' + match));
        log(colors.green(' ✔        ' + $config.url_replace + pattern[2] + '\n'));
      }
      // return match;
    }));
}


/** ::::::: HTML - Beautify */

function htmlBeautify () {
  return src([
    './.tmp/*.html',
    './.tmp/*.php'
  ])
    .pipe(plumber())
    .pipe(beautifyCode({
      config: './.jsbeautifyrc'
    }))
    .pipe(strip())
    .pipe(dest('./.tmp'));
}

module.exports = {
  htmlReplaceVars,
  htmlInclude,
  htmlInline,
  htmlSearch,
  htmlBeautify,
}
