/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const fs = require('fs-extra');
const del = require('del');
const deleteEmpty = require('delete-empty');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Clean
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

function delTemp () {
  return del([
    './.tmp/**'
  ], { dot: true });
}

function delDist () {
  return del([
    './dist/*'
  ], { dot: true });
}

function delLocal () {
  return del([
    './local/*'
  ], { dot: true });
}

function delFiles () {
  return del([
    './files/*'
  ], { dot: true });
}

function delSrc () {
  return del([
    './src/*'
  ]);
}

function delImg () {
  return del([
    './.tmp/images/*'
  ]);
}

function delHtml () {
  return del([
    './.tmp/*.html'
  ]);
}

function delPhp () {
  return del([
    './.tmp/*.php'
  ]);
}

function delCss () {
  return del([
    './.tmp/*.scss'
  ]);
}

function delJs () {
  return del([
    './.tmp/*.js'
  ]);
}

function delAll () {
  return del([
    './dist/',
    './build-*',
    './**/.DS_Store',
    './**/.*.tmp',
    './.tmp/',
    'package-lock.json',
    'yarn.lock'
  ])
    .then(() => {
      deleteEmpty.sync('./dist/');
    });
}

module.exports =  {
  delTemp,
  delDist,
  delLocal,
  delFiles,
  delSrc,
  delImg,
  delHtml,
  delPhp,
  delCss,
  delJs,
  delAll
}
