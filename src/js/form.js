/* global $, console, _bcvma, pageViewer, utag */
/* eslint no-undef: "error", semi: 2 */
/* jshint esversion: 6 */

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Set Redirect URL
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

var $redirectUrl;
var $redirectUrlId;


/** ::::::: Validate Meta Tag - Redirect URL */
$(document).ready(function () {
  if ($('meta[name=redirect-url]').length) {
    $redirectUrl = $('meta[name=redirect-url]').attr('content');
  } else {
    $redirectUrl = 'NO-META-TAG';
  }

  if ($redirectUrl === 'CHANGE_THIS' || $redirectUrl === 'NO-META-TAG' || $redirectUrl.length === 0 || !ValidURL($redirectUrl)) {
    $redirectUrl = '!!! ERROR !!!: No Meta Tag for Redirect Defined - Please add Meta Tag with valid URL';
  } else {
    $redirectUrl = $('meta[name="redirect-url"]').attr('content');
  }

  $redirectUrlId = $redirectUrl + '?aid=' + $aid;

  $('input[name="redirect-url"]').val($redirectUrlId);
});

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Validate Form
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

$(document).ready(function () {
  // Remove default submit event
  $('.elq-form').removeAttr('onSubmit');

  // Change default type of inputs
  $('[name="emailAddress"]').attr('type', 'email');

  $('[name="phone_number"]').parsley({
    pattern: '^[0-9]+$'
  });

  $('[name="activity-id"]').closest('.grid-layout-col').hide();
  $('[name="redirect-url"]').closest('.grid-layout-col').hide();
  $('[name="source"]').closest('.grid-layout-col').hide();
  $('[name="medium"]').closest('.grid-layout-col').hide();
  $('[name="channel"]').closest('.grid-layout-col').hide();
  $('[name="url"]').closest('.grid-layout-col').hide();

  $('[name="activity-id"]').parsley({ required: false });
  $('[name="redirect-url"]').parsley({ required: false });
  $('[name="source"]').parsley({ required: false });
  $('[name="medium"]').parsley({ required: false });
  $('[name="channel"]').parsley({ required: false });
  $('[name="url"]').parsley({ required: false });
  $('[name="OptIn"]').parsley({ required: false });

  function utagLink () {
    utag.link({
      tealium_event: 'eloqua form submit',
      customer_email: document.getElementsByName('emailAddress')[0].value
    });
  }

  $('.elq-form').parsley({
    required: true,
    trigger: 'focusout',
    errorsWrapper: '<span></span>',
    errorTemplate: '<span class="msg-is-invalid"></span>'
  })
    .on('form:submit', function () {
      utagLink();
    });
});

/**
  * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
  * ·······  Grap all URL Parameters - UTM + Select Dropdowns
  * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

var $originUrl = window.location.href;

$(document).ready(function () {
  $('[name="origin-url"]').val($originUrl);
  $('.elq-item-select').wrap('<div class="select-wrap"></div>');
  $('select[name=VerticalSegment]').html($selectIndustry);
  $('select[name=country]').html($selectCountry);
  $('[name="OptIn"] + label').html($privacyPolicy);
  $('input[type="Submit"]').val($submintBtn);
  $('input[name="activity-id"]').val($aid);
  console.log('%c --- Activity Id: ' + $aid, 'color: #0c6b00');
  $('input[name="source"]').val($allParameters.utm_source);
  $('input[name="medium"]').val($allParameters.utm_medium);
  $('input[name="channel"]').val($allParameters.utm_channel);
});
