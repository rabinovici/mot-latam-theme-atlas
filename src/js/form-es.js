/**
 * ::::::: Industries
 */

var $selectIndustry = '' +
'<option value="" selected="selected">Por favor seleccione</option>' +
'<option value="Education">Educación</option>' +
'<option value="Fire and Emergency Medical Services">Servicios médicos de emergencia y bomberos</option>' +
'<option value="Healthcare">Cuidado de la salud</option>' +
'<option value="Hospitality and Retail">Hospitalidad y venta al por menor</option>' +
'<option value="Manufacturing">Fabricación</option>' +
'<option value="Mining, Oil and Gas">Minería, petróleo y gas</option>' +
'<option value="National Government Security">Seguridad del gobierno nacional</option>' +
'<option value="Police">Policía</option>' +
'<option value="Public Services (non-Public Safety Government)">Servicios públicos (Gobierno de seguridad no pública)</option>' +
'<option value="Telecommunications, Finance, and General Management Services">Servicios de telecomunicaciones, finanzas y administración general</option>' +
'<option value="Transportation and Logistics">Transporte y logística</option>' +
'<option value="Utilities">Electricidad , Agua</option>';


/**
 * ::::::: Countries
 */

var $selectCountry = '' +
  '<option value=""> Por favor seleccione </option>' +
  // '<option value="BR"> Brazil </option>'
  '<option value="AR">Argentina</option>' +
  '<option value="BO">Bolivia</option>' +
  '<option value="CL">Chile</option>' +
  '<option value="CO">Colombia</option>' +
  '<option value="CR">CostaRica</option>' +
  '<option value="EC">Ecuador</option>' +
  '<option value="SV">El Salvador</option>' +
  '<option value="GT">Guatemala</option>' +
  '<option value="HN">Honduras</option>' +
  '<option value="MX">Mexico</option>' +
  '<option value="NI">Nicaragua</option>' +
  '<option value="PA">Panama</option>' +
  '<option value="PY">Paraguay</option>' +
  '<option value="PE">Peru</option>' +
  '<option value="DO">Republica Dominicana</option>' +
  '<option value="PR">Puerto Rico</option>' +
  '<option value="UY">Uruguay</option>' +
  '<option value="VE">Venezuela</option>';

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Privacy Policy
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

var $privacyPolicy = 'Sí, me gustaría recibir actualizaciones, noticias de productos y materiales promocionales de motorola solutions. Por favor, revise nuestra <a href="https://www.motorolasolutions.com/es_xl/about/privacy-policy.html" target="_blank" data-uet="{\'link-type\':\'link\',\'link-label\':\'declaración de privacidad\',\'page-area\':\'2\'}">declaración de privacidad</a> para obtener más detalles.';

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Submit btn
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

var $submintBtn = 'ENVIAR';


/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Error Message ES
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

Parsley.addMessages('es', {
  defaultMessage: 'Este valor parece ser inválido.',
  type: {
    email: 'Este valor debe ser un correo válido.',
    url: 'Este valor debe ser una URL válida.',
    number: 'Este valor debe ser un número válido.',
    integer: 'Este valor debe ser un número válido.',
    digits: 'Este valor debe ser un dígito válido.',
    alphanum: 'Este valor debe ser alfanumérico.'
  },
  notblank: 'Este valor no debe estar en blanco.',
  required: 'Este valor es requerido.',
  pattern: 'Este valor es incorrecto.',
  min: 'Este valor no debe ser menor que %s.',
  max: 'Este valor no debe ser mayor que %s.',
  range: 'Este valor debe estar entre %s y %s.',
  minlength: 'Este valor es muy corto. La longitud mínima es de %s caracteres.',
  maxlength: 'Este valor es muy largo. La longitud máxima es de %s caracteres.',
  length: 'La longitud de este valor debe estar entre %s y %s caracteres.',
  mincheck: 'Debe seleccionar al menos %s opciones.',
  maxcheck: 'Debe seleccionar %s opciones o menos.',
  check: 'Debe seleccionar entre %s y %s opciones.',
  equalto: 'Este valor debe ser idéntico.'
});

Parsley.setLocale('es');

