/**
 * ::::::: Industries
 */

var $selectIndustry = '' +
'<option value="">Por favor selecione</option>' +
'<option value="Education">Educação</option>' +
'<option value="Fire and Emergency Medical Services">Serviços Médicos de Incêndio e Emergência</option>' +
'<option value="Healthcare">Cuidados de saúde</option>' +
'<option value="Hospitality and Retail">Hospitalidade e varejo</option>' +
'<option value="Manufacturing">Fabricação</option>' +
'<option value="Mining, Oil and Gas">Mineração, Petróleo e Gás</option>' +
'<option value="National Government Security">Segurança do governo nacional</option>' +
'<option value="Police">Polícia</option>' +
'<option value="Public Services (non-Public Safety Government)">Serviços Públicos (governo não público de segurança)</option>' +
'<option value="Telecommunications, Finance, and General Management Services">Telecomunicações, Finanças e Serviços de Gestão Geral</option>' +
'<option value="Transportation and Logistics">Transporte e Logística</option>' +
'<option value="Utilities">Eletricidade, Água</option>';

/**
 * ::::::: Countries ES PT
 */

var $selectCountry = '' +
  '<option value="BR"> Brazil </option>';

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Privacy Policy
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */


var $privacyPolicy = 'Sim, eu gostaria de receber atualizações, notícias do produto e materiais promocionais das soluções da Motorola Solutions. Por favor, revise a nossa <a href="https://www.motorolasolutions.com/pt_xl/about/privacy-policy.html" target="_blank" data-uet="{\'link-type\':\'link\',\'link-label\':\'política de privacidade\',\'page-area\':\'2\'}">declaração de privacidade</a> para mais detalhes.';

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Submit btn
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

var $submintBtn = 'ENVIAR';


/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Error Message PT
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */
Parsley.addMessages('pt-br', {
  defaultMessage: 'Este valor parece ser inválido.',
  type: {
    email: 'Este campo deve ser um email válido.',
    url: 'Este campo deve ser um URL válida.',
    number: 'Este campo deve ser um número válido.',
    integer: 'Este campo deve ser um inteiro válido.',
    digits: 'Este campo deve conter apenas dígitos.',
    alphanum: 'Este campo deve ser alfa numérico.'
  },
  notblank: 'Este campo não pode ficar vazio.',
  required: 'Este campo é obrigatório.',
  pattern: 'Este campo parece estar inválido.',
  min: 'Este campo deve ser maior ou igual a %s.',
  max: 'Este campo deve ser menor ou igual a %s.',
  range: 'Este campo deve estar entre %s e %s.',
  minlength: 'Este campo é pequeno demais. Ele deveria ter %s caracteres ou mais.',
  maxlength: 'Este campo é grande demais. Ele deveria ter %s caracteres ou menos.',
  length: 'O tamanho deste campo é inválido. Ele deveria ter entre %s e %s caracteres.',
  mincheck: 'Você deve escolher pelo menos %s opções.',
  maxcheck: 'Você deve escolher %s opções ou mais',
  check: 'Você deve escolher entre %s e %s opções.',
  equalto: 'Este valor deveria ser igual.'
});

Parsley.setLocale('pt-br');
